module WPDB
  class RegistrationLog < Sequel::Model(:"#{WPDB.prefix}registration_log")
    #returns the blog id of the most recent wp blog to be created by admin email
    def self.get_last_blog_id_for_admin_email(email)
      RegistrationLog.where(:email => email).order(:id).reverse_order.get(:blog_id)
    end
  end
end
